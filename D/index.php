<?php

class EmailService
{
    public function sendEmail(string $message): void
    {
        echo "Sending email: $message\n";
    }
}

class TelegramService
{
    public function sendMessage(string $message): void
    {
        echo "Sending telegram: $message\n";
    }
}

class NotificationTelegram
{
    private TelegramService $telegramService;

    public function __construct()
    {
        $this->telegramService = new TelegramService();
    }

    public function send(string $message): void
    {
        $this->telegramService->sendMessage($message);
    }
}

class Notification
{
    private EmailService $emailService;

    public function __construct()
    {
        $this->emailService = new EmailService();
    }

    public function send(string $message): void
    {
        $this->emailService->sendEmail($message);
    }
}

$notification = new Notification();
$notification->send("Hello, World!");

$notificationTelegram = new NotificationTelegram();
$notificationTelegram->send('Telegram' . PHP_EOL);
