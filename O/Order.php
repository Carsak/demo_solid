<?php

class Order
{
    private $items = [];

    public function addItem($item, $price)
    {
        $this->items[] = ['item' => $item, 'price' => $price];
    }

    public function calculateTotal()
    {
        $total = 0;
        foreach ($this->items as $item) {
            $total += $item['price'];
        }
        return $total;
    }
}
