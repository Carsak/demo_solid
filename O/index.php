<?php
require 'Order.php';


// Пример использования стандартного расчета
$order = new Order();
$order->addItem('Item 1', 100);
$order->addItem('Item 2', 200);

echo "Total: " . $order->calculateTotal() . "\n";