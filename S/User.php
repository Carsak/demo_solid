<?php

class UserAccount
{
    private $username;
    private $password;
    private $email;

    public function __construct($username, $password, $email)
    {
        $this->username = $username;
        $this->password = $password;
        $this->email = $email;
    }

    public function validateEmail()
    {
        echo "Email is validated\n";
    }

    public function hashPassword()
    {
        $hash = sha1($this->password);
        echo "Password is hashed $hash\n";
    }

    public function saveToDatabase()
    {
        echo "User $this->username is saved to the database\n";
    }

    public function sendWelcomeEmail()
    {
        echo "Welcome email sent to $this->email\n";
    }
}

