<?php
require_once 'User.php';

// Пример использования
$user = new UserAccount('My name', 'password123', 'name@example.com');
$user->validateEmail();
$user->hashPassword();
$user->saveToDatabase();
$user->sendWelcomeEmail();