<?php
require_once 'Eagle.php';
require_once 'Penguin.php';

$birds = [new Eagle(), new Penguin()];

$result = [];
foreach ($birds as $bird) {
    $result[] = $bird->fly();
}

expectingStrings($result);

function expectingStrings(array $result)
{
    foreach ($result as $item) {
        if (is_string($item)) {
            var_dump($item);
        }
    }
}
