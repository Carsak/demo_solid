<?php
declare(strict_types=1);
require_once 'Bird.php';

class Penguin extends Bird
{
    const I_CANT_FLY = 2;

    public function fly(): string
    {
        return self::I_CANT_FLY;
    }
}
