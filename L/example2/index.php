<?php
require_once 'Eagle.php';
require_once 'Penguin.php';

$birds = [new Eagle(), new Penguin()];

$result = [];
foreach ($birds as $bird) {
    try {
        $result[] = $bird->fly();
    } catch (CantFlyException $exception) {
        $result[] = 'This bird cant fly';
    }
}

var_dump($result);

// далее по коду что-то делается с $result
