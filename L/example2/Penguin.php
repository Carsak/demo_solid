<?php
require_once 'Bird.php';

class Penguin extends Bird
{
    public function fly(): string
    {
        throw new CantFlyException('Penguins cant fly');
    }
}

class CantFlyException extends Exception
{

}
