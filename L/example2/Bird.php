<?php

abstract class Bird
{
    abstract public function fly(): string;
}

