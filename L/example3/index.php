<?php
require_once 'Eagle.php';
require_once 'Penguin.php';
require_once 'Dove.php';

$birds = [new Eagle(), new Penguin(), new Dove()];

expectingOnlyFlyableBirds($birds);


function expectingOnlyFlyableBirds(array $birds)
{
    foreach ($birds as $bird) {
        echo "Bird {$bird->name} can fly" . PHP_EOL;
    }
}

// Выведет,
// Bird Eagle can fly
// Bird Penguin can fly
// Bird Dove can fly
// что некорректно. Пингвины не могут летать.
// Перепишите код, чтобы функция expectingOnlyFlyableBirds выводила только летающих птиц.
// Можете даже несколько вариантов предложить V1, V2, V3 ... и тд
