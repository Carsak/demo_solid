<?php

abstract class Bird
{
    public string $name;

    abstract public function fly(): string;
}

