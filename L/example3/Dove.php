<?php
require_once 'Bird.php';

class Dove extends Bird
{
    public string $name = 'Dove';
    public function fly(): string
    {
        return "Dove is flying";
    }
}