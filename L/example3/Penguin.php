<?php
require_once 'Bird.php';

class Penguin extends Bird
{
    public string $name = 'Penguin';

    public function fly(): string
    {
        throw new Exception('Penguins cant fly');
    }
}
