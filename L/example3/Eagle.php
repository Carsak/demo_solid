<?php
require_once 'Bird.php';


class Eagle extends Bird
{
    public string $name = 'Eagle';

    public function fly(): string
    {
        return "Eagle is flying";
    }
}

