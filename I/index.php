<?php

interface OrderInterface
{
    public function placeOrder(): void;
    public function cancelOrder(): void;
    public function trackOrder(): string;
    public function deliverOrder(): void;
}

class OnlineOrder implements OrderInterface
{
    public function placeOrder(): void
    {
        echo "Order placed online.\n";
    }

    public function cancelOrder(): void
    {
        echo "Order cancelled online.\n";
    }

    public function trackOrder(): string
    {
        return "Tracking online order.";
    }

    public function deliverOrder(): void
    {
        echo "Delivering online order.\n";
    }
}

class InStoreOrder implements OrderInterface
{
    public function placeOrder(): void
    {
        echo "Order placed in store.\n";
    }

    public function cancelOrder(): void
    {
        echo "Order cancelled in store.\n";
    }

    public function trackOrder(): string
    {
        // In-store orders can't be tracked
        return "In-store orders cannot be tracked.";
    }

    public function deliverOrder(): void
    {
        // In-store orders are picked up by customers
        throw new Exception("In-store orders are picked up by customers.");
    }
}

$onlineOrder = new OnlineOrder();
$inStoreOrder = new InStoreOrder();

$onlineOrder->placeOrder();
$onlineOrder->cancelOrder();
$trackNumber = $onlineOrder->trackOrder();
echo $trackNumber;
$onlineOrder->deliverOrder();

$inStoreOrder->placeOrder();
$inStoreOrder->cancelOrder();
$trackNumber = $inStoreOrder->trackOrder();
echo $trackNumber;
$inStoreOrder->deliverOrder();